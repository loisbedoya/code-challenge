from helpers.bot_webservice import Webservice
import unittest


class WebserviceTest(unittest.TestCase):
    
    def test_parse_commands(self):
        input = "/help"
        parser = Webservice(input)
        command = parser.parse(input)
        self.assertEqual(command, 'help')

        command = parser.parse("/stock=test")
        self.assertEqual(command, 'test')

        command = parser.parse("/stock=aapl.us")
        self.assertEqual(command, 'aapl.us')

        command = parser.parse("anything without / at begin")
        self.assertEqual(command, None)


if __name__ == "__main__":
    unittest.main()