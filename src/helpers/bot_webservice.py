
import requests
import csv
import re
import logging

logging.basicConfig(level=logging.INFO)

class Webservice:

    def __init__(self, message):
        self.message = message
        self.stock_id = ''

    def call(self, message):
        self.stock_id = message
        text = self.download()
        return self.parse_response(text)

    def download(self):
        url = f'https://stooq.com/q/l/?s={self.stock_id}&f=sd2t2ohlcv&h&e=csv'
        try:
            r = requests.get(url)
        except Exception as ex:
            logging.exception(ex)
            return None

        return r.text

    def parse_response(self, text):
        lines = text.splitlines()
        contents = csv.reader(lines, delimiter=',')
        rows = list(contents)
        if len(rows) < 2:
            return None
        if len(rows[1]) < 6:
            return None
        value = rows[1][6]
        return f'{self.stock_id.upper()} quote is ${value} per share'


    def parse(self, message):
        result = re.compile("^/stock=(?P<command>\S+)$", re.I).search(message)
        if result and 'command' in result.groupdict().keys():
            return result.groupdict()['command']
        result = re.compile("^/help$", re.I).search(message)
        if result:
            return 'help'
        elif self.is_command(message):
            self.stock_id = 'unknown'
            return 'unknown'
        return None

    def is_command(self, message):
        if message[0] == "/":
            return True
        return False

    def close(self):
        self.session.close()

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()