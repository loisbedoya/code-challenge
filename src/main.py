import json
import bottle
import time
import os
import sys
from bottle import route, error, request, response, static_file, auth_basic
from helpers.redis_datastore import RedisDataStore
from helpers.message_broker.producer import RabbitMQProducer
import configparser
from importlib import reload
from bottle import Bottle,response
import hashlib, binascii, os

reload(sys)
current_dir = os.path.dirname(__file__)
ROOT_DIR=os.path.abspath(os.path.join(current_dir, '..'))

def allow_cors(func):
    """ this is a decorator which enable CORS for specified endpoint """
    def wrapper(*args, **kwargs):
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
        if bottle.request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return func(*args, **kwargs)

    return wrapper

def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512', 
                                  provided_password.encode('utf-8'), 
                                  salt.encode('ascii'), 
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password

def login(username, password):
    key = 'chat:users:' + username
    redis = RedisDataStore()
    value = redis.get_value(key)

    if value is None:
        return False
    
    if verify_password(value.decode("utf-8"), password):
        return True
    return False

@error(404)
@error(401)
@error(400)
@error(500)
def errorXXX(err):
    response.headers['Content-type'] = 'application/json'
    response.headers['Access-Control-Allow-Origin'] = '*'
    return json.dumps({
        'error': {
            'http_code': err.status_code,
            'message': err.body
        }
    })


@route('/api/chatrooms', method=['OPTIONS', 'GET'])
def list_chatrooms():
    response.headers['Content-type'] = 'application/json'
    return json.dumps(['Chatroom1', 'Chatroom2', 'Chatroom3', 'Chatroom4', 'Chatroom5', 'Chatroom6'])


@route('/api/chatrooms/<chatroom_id>/messages', method=['OPTIONS', 'GET'])
def get_messages(chatroom_id):
    response.headers['Content-type'] = 'application/json'
    redis = RedisDataStore()
    key = 'chat:' + chatroom_id.lower() + ':messages'
    messages = redis.lrange(key, 0, 50)
    #casting from bytes to string
    for i in range(len(messages)):
        messages[i] = messages[i].decode("utf-8") 
    return json.dumps(messages)


@route('/api/chatrooms/<chatroom_id>/messages', method=['OPTIONS', 'POST'])
def new_message(chatroom_id):
    response.headers['Content-type'] = 'application/json'
    obj = request.json
    payload = {
        'm': obj['m'],
        't': time.time(),
        'r': str(chatroom_id),
        'u': request.auth[0],
    }
    value = json.dumps(payload)

    key = 'chat:' + chatroom_id.lower() + ':messages'
    redis = RedisDataStore()
    redis.rpush(key, value)
    producer = RabbitMQProducer('commands')
    producer.queue(value)
    producer.stop()
    return value


@route('/fonts/<filename>')
def serve_static_fonts(filename):
    return static_file(filename, root=os.path.join(ROOT_DIR, 'static', 'fonts'))


@route('/css/<filename>')
def serve_static_css(filename):
    return static_file(filename, root=os.path.join(ROOT_DIR, 'static', 'css'))


@route('/js/<filename>')
def serve_static_js(filename):
    return static_file(filename, root=os.path.join(ROOT_DIR, 'static', 'js'))


@route('/')
@auth_basic(login)
def app_homepage(filename='index.html'):
    return static_file(filename, root=os.path.join(ROOT_DIR, 'static'))

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('config.config')
    host = config['server']['host']
    port = int(config['server']['port'])
    app = bottle.app()
    app.run(host=host, port=port, debug=True)
