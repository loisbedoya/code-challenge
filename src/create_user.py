import logging
from helpers.redis_datastore import RedisDataStore
import hashlib, binascii, os

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), 
                                salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')

if __name__ == '__main__':
    username = input('New username: ')
    password = input('New password: ')

    key = 'chat:users:' + username
    redis = RedisDataStore()
    value = redis.get_value(key)    

    if value is None:
        #hashed = bcrypt.hashpw(password.encode('utf8'), CONFIG.get('server', 'password-salt').encode('utf8'))
        #redis.set_value(key, hashed)
        #redis.set_value(key, password.encode('utf8'))
        redis.set_value(key, hash_password(password))

    else:
        LOGGER.info(' {} already exists.'.format(username))
