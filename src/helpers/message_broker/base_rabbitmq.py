
import pika
import configparser


class BaseRabbitMQ(object):

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.config')
        credentials = pika.PlainCredentials(
            config['rabbitmq']['username'],
            config['rabbitmq']['password']
        )
        parameters = pika.ConnectionParameters(
            credentials=credentials,
            host=config['rabbitmq']['host'],
            port=int(config['rabbitmq']['port']),
            virtual_host=str(config['rabbitmq']['vhost'])
        )
        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()

    def stop(self):
        self.channel.stop_consuming()

