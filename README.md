**Installation**

It is required to have installed on your local machine:


* Python 3


* RabbitMQ
    MAC OS ```brew install rabbitmq```
    LINUX https://www.rabbitmq.com/install-debian.html#apt
    
    
* Redis
    MAC OS ```brew install redis```
    LINUX ```apt install redis``` ```apt install redis-server```

Create a virtual environment ``` virtualenv env```


Activate the environment ```. env/bin/activate```



The project requires the following Python packages to be installed (Python 3):
    * bottle
    * redis
    * requests
    * pika
    * redis-server

All of these are installed via pip (or pip3 if the case), further more, a requirements.txt file is provided so install the packages via:
Install requirements ```pip install -r requirements.txt```

**How to use**

Please verify your services redis and rabbitMQ are running, run on terminal:    
    MAC OS  ```redis-server```     
            ```brew services restart rabbitmq```                    
            ```brew services restart redis```
    LINUX Please check official documentation

    

To start create a user with which you will enter the chat by running manually. If you have already created a user, this step is not necessary.
    ```python src/create_user.py```

After this start the chat server
    ```./run.sh```

The chat server and the bot in charge of get the quotes data can be run manually on two consoles:
    ```python src/bot.py```    
    ```python src/main.py```
    

To run unittest just run the tests.py file.
    ```python src/tests.py```    
    

Go to 127.0.0.1:5000 and login with the user you created and chat!


---

