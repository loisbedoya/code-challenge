import json
import time
from helpers.message_broker.consumer import RabbitMQConsumer
from helpers.redis_datastore import RedisDataStore
from helpers.bot_webservice import Webservice
import logging


class Consumer(RabbitMQConsumer):

    def __init__(self, queue):
        RabbitMQConsumer.__init__(self, queue)

    def callback(self, ch, method, properties, body):
        payload = json.loads(body)
        chatroom = payload['r']
        message = payload['m']

        super(Consumer, self).callback(ch, method, properties, body)

        try:
            webservice = Webservice(message)
            command = webservice.parse(message)
            if command == 'help':
                response = 'Please type "/stock=stock_code" to get stock quote.'
            elif command == 'unknown':
                response = 'Unknown command, please type /help to see the options.'
            elif command is None:
                return
            else:
                response = webservice.call(command)
        except Exception as ex:
            logging.exception(ex)

        payload = {
            'u': 'Bot',
            'm': response,
            't': time.time(),
            'r': chatroom
        }
        value = json.dumps(payload)
        key = 'chat:' + chatroom.lower() + ':messages'
        redis = RedisDataStore()
        redis.rpush(key, value)


if __name__ == '__main__':
    consumer = Consumer('commands')

    try:
        consumer.run()
    except KeyboardInterrupt:
        consumer.stop()
