
import redis
import configparser

class RedisDataStore:

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.config')
        self.conn = redis.Redis(
            host = config['redis']['host'],
            port = int(config['redis']['port'])
        )

    def get_value(self, key):
        return self.conn.get(key)

    def set_value(self, key, value):
        self.conn.set(key, value)

    def rpush(self, key, value):
        self.conn.rpush(key, value)

    def lrange(self, key, start, stop):
        return self.conn.lrange(key, start, stop)

    def get_keys(self, key):
        return self.conn.keys(key)

    def hget(self, key, field):
        return self.conn.hget(key, field)

    def hset(self, key, field, value):
        self.conn.hset(key, field, value)
